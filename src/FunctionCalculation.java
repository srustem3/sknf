/**
 * SKNF
 * 11-602
 * Group 4
 */
public class FunctionCalculation {
    private static int countX = 3; // Кол-во переменных в функции

    private static boolean calculation(boolean[] x) {
        // return equivalence(x[0], x[1]);
        return (x[0] | implication(x[1], x[2])) & isklILI(x[0], x[1]); // Здесь вводится сама функция
    }

    public static void main(String[] args) {
        String x; // Текущая строчка таблицы истинности
        boolean xRes; // Результат выполнения функции на конкретной строчке таблицы истинности
        String s = ""; // Строчка с СКНФ (рез-тат)
        String sCurrent = ""; // Строчка с текущей скобкой СКНФ
        boolean[] xBoolean = new boolean[countX]; // Текущая строчка табл. ист. в виде булевского массива
        for (long i = 0; i < Math.pow(2, countX); i++) {
            x = leftPad(Long.toBinaryString(i)); // Преобразует конкретный i в двоичный формат (строка с цифрами) с добавлением лидирующих нулей
            for (int j = 0; j < countX; j++) {
                xBoolean[j] = x.charAt(j) == '1';
            }
            xRes = calculation(xBoolean);
            sCurrent = sknf(xBoolean, xRes);
            if (!(sCurrent.equals(""))) {
                if (!(s.equals(""))) {
                    s += " * ";
                }
                s += sCurrent;
            }
        }
        System.out.println(s);
    }

    private static String leftPad(String x) { // Добавление лидирующих нулей в таблицу истинности
        int length = x.length();
        for (int i = 1; i <= countX - length; i++) {
            x = "0" + x;
        }
        return x;
    }

    private static String sknf(boolean[] x, boolean xRes) { // Возвращает текущую скобку СКНФ
        String s = "";
        if (!xRes) {
            s += "(";
            for (int i = 0; i < x.length; i++) {
                if (x[i]) {
                    s += "!x" + i;
                    if (i != x.length - 1) {
                        s += " + ";
                    }
                } else {
                    s += "x" + i;
                    if (i != x.length - 1) {
                        s += " + ";
                    }
                }
            }
            s += ")";
        }
        return s;
    }
    // Методы для логических операций операции
    private static boolean implication(boolean x, boolean y) {
        return !x | y;
    }

    private static boolean equivalence(boolean x, boolean y) {
        return x & y | !x & !y ;
    }

    private static boolean isklILI(boolean x, boolean y) {
        return x & !y | !x & y;
    }

    private static boolean shtrih(boolean x, boolean y) {
        return !x | !y;
    }

    private static boolean strelka(boolean x, boolean y) {
        return !x & !y;
    }
}